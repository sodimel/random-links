# Random links

This small php page will redirect you on one random link from a file.

## Download & config

```bash
git clone https://gitlab.com/sodimel/random-links.git
```

Fill `index.php`) with relevant content:

 * `YOUR_URL_HERE` (do not set /index.php or a link will be `..../index.php/links.txt` and it's bad),
 * `PUT YOUR TOPICS HERE`

----

# Small list of instances

*Pls create an issue/MR with your website URL in order to be added to the list. No nsfw, no hate.*

 * https://misc.l3m.in/random/
