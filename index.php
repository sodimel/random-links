<?php

$SITE_URL = "YOUR_URL_HERE";

function getRandomLine($filename) {
    $lines = file($filename) ;
    return $lines[array_rand($lines)] ;
}

$url = getRandomLine("links.txt");
$nbLines = count(file("links.txt"));

if(isset($_GET['show'])){
    echo($url);
    exit();
}
if(isset($_GET['go'])){
    header("Location: ". $url);
    exit();
}
?>


<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Curated random links</title>
    <meta name="description" content="Curated random links from internet.">
    <link rel="icon" type="image/png" href="favicon.png" />
    <link rel="manifest" href="pwa.webmanifest">
    <style>
        html, body{
            width: 100%;
            min-height: 100%;
            padding: 0;
            max-width: 500px;
            background-color: #333;
            color: #ddd;
            font-family: sans-serif;
            margin: 0 auto;
        }
        main{
            margin: 5px;
            padding: 15px;
            background-color: #666;
            border-radius: 5px;
            box-sizing: border-box;
        }
        footer{
            font-size:  0.85em;
            opacity: 0.7;
        }
        footer h2{
            padding-top: 5px;
        }
        footer p{
            margin: 5px;
        }
        .center{
            text-align: center;
        }
        h1,h2{
            font-family: monospace;
            color:  #fff;
            text-align: center;
            max-width: 80%;
            margin: 5px auto;
        }
        h2{
            border-top: 1px solid #888;
            padding-top: 15px;
        }
        p{
            text-align: justify;
        }
        a{
            color: #ddf;
            text-shadow: 0 0 3px #57f;
        }
        a:visited{
            color: #ebf;
            text-shadow: 0 0 3px #b3e;
        }
        @media (max-width: 500px) {
            body{
                display: inherit;
            }
            main{
                height: 100%;
                border-radius: 0;
                padding-top: 50px;
            }
        }
    </style>
</head>
<body>
    <main>
        <h1><?php echo $nbLines; ?> Curated random links</h1>

        <section>
            <h2>Why?</h2>
            <p>
                I figured that instead of keeping my links in my "read later", "culture", "interwebz" and "gold content" bookmark folders just for me, I wanted to share them with other people.<br />
                Instead of spending time describing each link on a dedicated page, I thought I would create this page that returns a random link from my list.<br />
                Warning: some of the links are to French resources (this warning does not apply to you if you understand French).
            </p>
        </section>


        <section>
            <h2>How to access a random links?</h2>
            <p class="center">
                <a href="<?php echo $SITE_URL; ?>?go"><?php echo $SITE_URL; ?>?go</a>.
            </p>
        </section>

        <section>
            <h2>How to print a random links?</h2>
            <p class="center">
                <a href="<?php echo $SITE_URL; ?>?show"><?php echo $SITE_URL; ?>?show</a>.
            </p>
        </section>

        <section>
            <h2>How to print <i>all</i> the links?</h2>
            <p class="center">
                <a href="<?php echo $SITE_URL; ?>links.txt"><?php echo $SITE_URL; ?>links.txt</a>.
            </p>
        </section>

        <section>
            <h2>Topics</h2>
            <p>
                PUT YOUR TOPICS HERE
            </p>
        </section>

        <footer>
            <h2>Random notes</h2>
            <p>
                This website is a PWA, you can "install it" on your phone (add it on your home screen). Only tested on Android using Chrome & Firefox.<br />
                There's no js on this website because we don't need js. The css is handmade too. Use firefox instead of chrome. You have things to hide, otherwise I would already have access to all your emails and administrative documents. Fork this project <a href="https://gitlab.com/sodimel/random-links">on gitlab</a>.
            </p>
        </footer>
    </main>
</body>
</html>
